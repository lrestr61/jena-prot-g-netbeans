
JENA PROTEGE NETBEANS
=====================
```
Teamwork:  Luisa Restrepo        date: 10/06/2014
``` 
**Description**: 

Programa que lee una ontologia .owl creada en protégé, en Netbeans se agregaron las librerias de Jena para manipular la ontologia. El programa lee la ontologia y hace sparql, dando los resultados, la interfaz muestra los individuos de la ontologia.


**Usage**:

Para su correcto funcionamiento, cambie la URL en la clase cargarOntologia.class.

