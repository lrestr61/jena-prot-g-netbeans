/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package menuontology;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.FileManager;
import java.io.InputStream;



/**
 *
 * @author Luisa Restrepo
 */
public class cargarOntology {
    
public static class Monitoring {

        static OntModel model;

        /** Returns an instance of the monitoring ontology model;  */
        public static OntModel getModel() {
            if (model == null)
                load();

            return model;
        }

        /** Helper function for loading our ontology from file*/
        private static void load() {
            // Create an empty model
            Model m = ModelFactory.createDefaultModel();

            // Use the FileManager to find the input file
            InputStream in = FileManager.get().open("C:\\Users\\Luisa Restrepo\\Dropbox\\8 SEMESTRE\\Ingenieria del Conocimiento\\Trabajo\\Segunda Ontologia - RestriccionAlimenticia\\RestriccionesAlimenticiasFinal.owl");
            //Error al no encontrar el archivo
            if(in==null){throw new IllegalArgumentException("Archivo no encontrado");}
            // Read the RDF/XML ontology file that we created with Protegé
            m.read(in, null);

            model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, m);
        }
    }
    
}
