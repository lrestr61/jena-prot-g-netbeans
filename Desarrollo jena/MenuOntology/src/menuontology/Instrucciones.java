/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package menuontology;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;


/**
*
* @author Luisa Restrepo
*/
public class Instrucciones {

static OntModel model = cargarOntology.Monitoring.getModel();

public static void crearPersona(){
 Resource classification;
 Individual person;
 //Se pregunta por la clase
 classification = model.getResource(URI.Monitoring + "Persona");
 //Se crea el individuo
 person = model.createIndividual(URI.Monitoring + "Persona:Andrea", classification);
 //ubicamos la data-property que representa el nombre
 Property name = model.getProperty(URI.Monitoring + "name");
 //se unen el nombre de la persona es
 person.addProperty(name, "Andrea");
}

public class URI{
//url de la ontologia
public static final String Monitoring = "http://www.semanticweb.org/andrea/ontologies/2014/5/untitled-ontology-5#";
}


public static String listarIndividuos(){

String content="";
ExtendedIterator<Individual> individuals = model.listIndividuals();

 while(individuals.hasNext())
    {
        Individual individual = individuals.next();
        // Retrieves the URI - identifier of the individual
        String label = individual.getLocalName();
        // Retrieves the class name of the individual
        String type = individual.getLocalName();
        content += label + "\n" ;
    }
    System.out.println("Intividuals present in the ontology: \n" + content);
    return content;
}


public static void queryaaa(){

String statement =     
       "PREFIX owl:  <http://www.w3.org/2002/07/owl#>\n" + 
                " SELECT * " +
                " WHERE " +
                " { " +
                //Imprime las uri de las clases de la ontologia
                " ?URI a owl:Class . " +
                " } " ;
Query mQuery = QueryFactory.create(statement);
QueryExecution mQueryExecution = QueryExecutionFactory.create(mQuery, model);
ResultSet results = mQueryExecution.execSelect();
ResultSetFormatter.out(System.out, results, mQuery);
mQueryExecution.close();

}

public static void comidasSinRestriccion(){
    
System.out.println("Restricciones con alimentos permitidos");
String statement;
    statement = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"+
                "PREFIX owl: <http://www.w3.org/2002/07/owl#> \n"+
                "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n"+
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"+
                "PREFIX ont: <http://www.semanticweb.org/andrea/ontologies/2014/5/OntologiaFinal#> \n"+
                "SELECT DISTINCT ?Restriccion_Alimenticia ?Comidas_Aceptadas \n"+
                "WHERE { ?Restriccion_Alimenticia a ont:Restricciones ; ont:hasTipoComida ?Comidas_Aceptadas. }";
               

Query mQuery = QueryFactory.create(statement);
QueryExecution mQueryExecution = QueryExecutionFactory.create(mQuery, model);
ResultSet results = mQueryExecution.execSelect();
ResultSetFormatter.out(System.out, results, mQuery);
mQueryExecution.close() ;

//return results.next().toString();
}

public static void comidasConRestriccion(){
    
System.out.println("Restricciones con alimentos NO permitidos");

String statement;
    statement = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"+
                "PREFIX owl: <http://www.w3.org/2002/07/owl#> \n"+
                "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n"+
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"+
                "PREFIX ont: <http://www.semanticweb.org/andrea/ontologies/2014/5/OntologiaFinal#> \n"+
                "SELECT DISTINCT ?Restriccion_Alimenticia ?Comidas_Aceptadas \n"+
                "WHERE { ?Restriccion_Alimenticia a ont:Restricciones ; ont:hasRestriccion ?Comidas_Aceptadas. }";
               

Query mQuery = QueryFactory.create(statement);
QueryExecution mQueryExecution = QueryExecutionFactory.create(mQuery, model);
ResultSet results = mQueryExecution.execSelect();
ResultSetFormatter.out(System.out, results, mQuery);
mQueryExecution.close() ;

//return results.next().toString();
}


        
public static void listarRestricciones(){

System.out.println("Restricciones en la Ontologia");
String statement;
    statement = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"+
                "PREFIX owl: <http://www.w3.org/2002/07/owl#> \n"+
                "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n"+
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"+
                "PREFIX ont: <http://www.semanticweb.org/andrea/ontologies/2014/5/OntologiaFinal#> \n"+
                "SELECT ?Restriccion_Alimenticia  \n"+
                "WHERE { ?Restriccion_Alimenticia a ont:Restricciones  }";
               

Query mQuery = QueryFactory.create(statement);
QueryExecution mQueryExecution = QueryExecutionFactory.create(mQuery, model);
ResultSet results = mQueryExecution.execSelect();
ResultSetFormatter.out(System.out, results, mQuery);
mQueryExecution.close() ;

//ResultSetFormatter.out(results);
}

public static void sad(){

System.out.println("Restricciones en la Ontologia");
String statement;
    statement = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"+
                "PREFIX owl: <http://www.w3.org/2002/07/owl#> \n"+
                "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n"+
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"+
                "PREFIX ont: <http://www.semanticweb.org/andrea/ontologies/2014/5/OntologiaFinal#> \n"+
                "SELECT ?nom ?ed  \n"+
                "WHERE { ?resource ont:Restricciones ?nom .  \n"+
                         "?resources ont:hasTipoComida ?ed  \n" +
                         "FILTER (?nom=Ulcera)  \n"+
                         "}";
               

Query mQuery = QueryFactory.create(statement);
QueryExecution mQueryExecution = QueryExecutionFactory.create(mQuery, model);
ResultSet results = mQueryExecution.execSelect();
ResultSetFormatter.out(System.out, results, mQuery);
mQueryExecution.close() ;




//ResultSetFormatter.out(results);
}

}
